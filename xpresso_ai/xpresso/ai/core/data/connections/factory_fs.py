""" Factory class for filesystem connectors in Data-Connectivity """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'

import enum
import xpresso.ai.core.data.connections.connector_exception as connector_exception
from xpresso.ai.core.data.connections.external import FSConnector
from xpresso.ai.core.data.connections.internal import LocalFSConnector


class FSType(enum.Enum):
    """

    Enum Class that lists types of file systems supported by FSConnector class

    """

    Alluxio = "Alluxio"
    Local = "Local"


class FSConnectorFactory:
    """

    Factory class to provide Connector object of specified type

    """

    @staticmethod
    def getconnector(datasource_type):
        """

        This method returns Connector object of a specific datasource

        :param datasource_type: a string object stating the
                                datasource

        :return: Connector object
        """

        if datasource_type == FSType.Local.value:
            return LocalFSConnector()
        else:
            return FSConnector()
