import abc
from enum import Enum
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.utils.constants import *
from xpresso.ai.core.data.visualization.report import ReportParam
from xpresso.ai.core.data.visualization import utils


class PlotType(Enum):
    """
    Enum class to standardize all the Plottype
    """
    PIE = "pie"
    QUARTILE = "quartile"
    BAR = "bar"
    HEATMAP = "heatmap"
    DENSITY = "density"
    SCATTER = "scatter"
    MOSAIC = "mosaic"

    def __str__(self):
        return self.value


class AbstractVisualize(metaclass=abc.ABCMeta):
    def __init__(self, dataset):
        self.dataset = dataset
        self.attribute_info = dataset.info.attributeInfo
        self.metric = dataset.info.metrics
        self.logger = XprLogger()

    @classmethod
    @abc.abstractmethod
    def render_univariate(cls, attr_name=None, attr_type=None, plot_type=None,
                          output_format=utils.HTML,
                          output_path=REPORT_OUTPUT_PATH, report=False,
                          report_format=ReportParam.SINGLEPAGE.value,
                          file_name=None, target=None):
        pass

    @classmethod
    @abc.abstractmethod
    def render_multivariate(cls, output_format=utils.HTML,
                            output_path=REPORT_OUTPUT_PATH,
                            report=False, file_name=None):
        pass

    @classmethod
    @abc.abstractmethod
    def render_all(cls, output_format=utils.HTML,
                   output_path=REPORT_OUTPUT_PATH, report=False,
                   report_format=ReportParam.SINGLEPAGE.value,
                   file_name=None, target=None):
        pass

    @staticmethod
    def set_axes_labels(x_label, y_label, target_label=EMPTY_STRING):
        """
        Sets axes labels
        Args:
            x_label(:str): String label for x axis
            y_label(:str): String label for y axis
            target_label(:str): String label for target variable
        Returns:
            Dictionary of x_label and y_label"""
        axes_labels = dict()
        axes_labels[utils.X_LABEL] = x_label
        axes_labels[utils.Y_LABEL] = y_label
        axes_labels[utils.TARGET_LABEL] = target_label
        return axes_labels
