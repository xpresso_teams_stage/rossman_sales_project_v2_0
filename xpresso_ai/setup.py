from setuptools import setup
from xpresso.ai.core.commons.utils.generic_utils import get_version

with open('requirements.txt', 'r') as f:
    requirements = f.read().splitlines()

# Setup configuration
setup(
    name='xprctl',
    version=get_version(),
    packages=['xpresso'],
    entry_points={
        'console_scripts': [
            'xprctl = xpresso.ai.client.xprctl:cli_options'
        ]
    },
    description="Python command line application bare bones template.",
    author="Naveen Sinha",
    author_email="naveen.sinha@abzooba.com",
    install_requires=requirements
)